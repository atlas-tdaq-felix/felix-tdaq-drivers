# felix-tdaq-drivers [![pipeline status](https://gitlab.cern.ch/atlas-tdaq-felix-dev/felix-tdaq-drivers/badges/main/pipeline.svg)](https://gitlab.cern.ch/atlas-tdaq-felix-dev/felix-tdaq-drivers/-/commits/main)

This repository is used to build the FELIX driver bundle in the form of:

1. **Module tarball**: a tarball containing driver modules (.ko files) compiled for different kernel version.\
   This is meant for FELIX PCs in P1 or TBED.
1. **Library tarball**: a tarball containing driver libraries (.so files) and headers.\
   The libraries and headers are to be included in the FELIX software release as external component.
1. **RPM**: the source RPM distributed to FELIX users that installas FELIX and CMEM modules.

Packages part of the TDAQ release are included as submodules and compiled withing the FELIX CMake environment.\
**Output files** are saved on EOS using the hierarchy shown below:
```
/eos/project/f/felix/www/user/dist/software/drivers/
├── latest
│   ├── centos7
│   └── centos9
└── releases
    ├── centos7
    └── centos9
        ├── felix-drivers-00-00-00
        │   ├── felix-drivers-00-00-00-ko-5.14.0-284.11.1.el9_2.x86_64.tar.gz
        │   ├── felix-drivers-00-00-00-lib-x86_64-centos9-gcc11-opt.tar.gz
        │   └── tdaq_sw_for_Flx-0.0.0-2dkms.noarch.rpm
```


#### Releases
Releases are built when a tag is created on this repository\
The tag shall use the format `felix-drivers-xx-xx-xx`.\
The label `felix-drivers-00-00-00` is reserved and used when a commit is pushed.

#### Scheduled "latest" builds
An automatic pipeline periodically builds the driver bundle. This build will pull the latest version of all submodules and save
the output in the "latest" directory using label `felix-drivers-00-00-00`. 

#### Scheduled builds of last tag
An automatic pipeline weekly rebuilds the last tag to keep it up to date with recent kernels.

[- When a new release is made by creating a new tag the pipeline schedule shall be manually edited to point to the new tag. -]
[- This can be done using the web interface. From the left panel: CI/CD > Schedules > Pencil button > change "Target branch or tag"  -]


#### AlmaLinux9 minor version updates
When CERN IT moves to a new AlmaLinux9 version (e.g. 9.3), the kernels for the previous version (9.2) are no longer available in the default `dnf` repositories

```
[kernel] https://linuxsoft.cern.ch/cern/alma/9/BaseOS/x86_64/os/Packages/
[kernel-dev] https://linuxsoft.cern.ch/cern/alma/9/AppStream/x86_64/os/Packages/
```
To install old kernels, [repo snapshots](https://linuxsoft.cern.ch/cern/alma/9-snapshots/) have to be added to the system (whether local or CI pipeline).
In this git repository, the  `dnf` repos files correspoding to useful snapshots are kept in [kernel_modules/repos](kernel_modules/repos). The build scripts in `kernel_modules` copy the approriate `dnf` repos, already enabled, in `/etc/yum.repos.d/`.

This feature allows to recompile drivers for the AlmaLinux9 kernels used in production, while CERN IT moves to newer OS versions.


# HowTos
##### Clone this repo
```
git clone <this repo>
git submodule init && git submodule update
```
##### Update a submodule
Once the repository has been cloned:
```
cd <submodule-X>
git pull origin master
cd ..
git commit -am "update of submodule-X
```

##### Update all submodules at once
Once the repository has been cloned:
```
git submodule update --remote
```

##### Re-run a module build of an old release
This operation is useful if kernel modules of an old release are needed for a new kernel.
In the web application select the old tag and navigate to the CI pipeline clicking on the green tick on the right.
Then re-run the module build stage: the build script in this repo will pick up new kernels.


# Implementation
The build process is split in two stages as libraries and kernel objects are compiled separately.
In both cases the [setup_tags.sh](setup_tags.sh) needs to be run to initialise the environment variables used
to keep track of versions. For a local build only RELEASE_NAME needs to be set manually.

| Variable | Use    |
|:--------|:--------|
| RELEASE_NAME    | Tag name from CI_COMMIT_TAG, defaults to felix-drivers-00-00-00 otherwise. To run locally set CI_COMMIT_TAG. |
| RELEASE_TAG     | Numberic string automatically extracted from RELEASE_NAME e.g. 4.12.0 from felix-drivers-04-12-00.             |
| GIT_BUILD_HASH  | `$(git describe --always --dirty)` used for Makefile's CVSTAG |
| GIT_RCD_HASH    | `$(cd ROSRCDdrivers && git describe --always --dirty)` used for Makefile's FLX_TAG.  |
| GIT_CMEM_HASH   | `$(cd cmem_rcc && git describe --always --dirty)` used for Makefile's CMEM_RCC_TAG |
| GIT_IO_HASH     | `$(cd io_rcc && git describe --always --dirty)` used for Makefile's to IO_RCC_TAG |


## Building libraries
Libraries are built using the FELIX CMake setup (submodules [cmake_tdaq](cmake_tdaq) - in the FELIX version - and [python_env](python_env)).
The CI build follows the standard procedure (to be run manually in a local build): `cmake_config && cd $BINARY_TAG && make && make install`.

For a local build:
```
source python_env/bin/activate
source cmake_tdaq/bin/setup.sh
source setup_tags.sh
cmake_config
cd $BINARY_TAG
make && make install
```

## Packaging: rpm & tar
The RPM is architecture-independent but contains libraries and executables for
debugging purposes and it also build separately for each operating system. 
In the CI The rpm and tarball are build on the CI in a clean environment
but retaining the files from the previous step.

The RPM is build by [build_rpm.sh](build_rpm.sh) that takes care of generating
dkms.conf and Makefile_drivers on-the-fly using a Python script so to include
the correct package versions.

The tarball pf libraries and headers is built by
[build_library_tarball.sh](build_library_tarball.sh)

For a local build open a new terminal and navigate to felix-tdaq-drivers, then
```
export BINARY_TAG=<binary tag from previous step, x86...>
export CMAKE_PREFIX_PATH=<same as in previous step>
source setup_tags.sh
mkdir -p dist && mkdir -p dist/$RELEASE_NAME
./build_rpm.sh
./build_library_tarball.sh
```

## Building modules
Modules are built using a clean environment using the compiler shipped with the OS.
Scripts as [kernel_modules/build_centos9.sh](kernel_modules/build_centos9.sh)
install the latest kernel source packages with dnf/yum and build the modules for
different kernel versions. The modules are then compressed in a tarball.

For a local build using the kernel installed in the system run: 
```
source setup_tags.sh
cd kernel_modules
source setup_kernel.sh
cd src
make
```
provided that the system has kernel, kernel-devel and grubby packages installed.
