#!/bin/bash

# Add the appropriate DNF repo
cp ${CI_PROJECT_DIR}/kernel_modules/repos/almalinux-baseos-9_2.repo /etc/yum.repos.d/
cp ${CI_PROJECT_DIR}/kernel_modules/repos/almalinux-appstream-9_2.repo /etc/yum.repos.d/

NUM_KERNELS=3
OS_VERSION=9_2
ALL_KERNELS=$(dnf list --showduplicates kernel | grep kernel.x86_64 | grep ${OS_VERSION} | awk '{print $2 ".x86_64"}' | tail -n ${NUM_KERNELS})

for KERNEL in ${ALL_KERNELS}; do
    dnf install -y kernel-${KERNEL} kernel-devel-${KERNEL}
    source setup_kernel.sh ${KERNEL}
    cd src
    make clean
    make
    if [ $? -ne 0 ]; then
        echo "Error: The make command exited with a non-zero status code."
        exit 1
    fi
    cd ..
done
