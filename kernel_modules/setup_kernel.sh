if [ $# -eq 0 ]; then
    echo "No kernel version passed. Using installed $(uname -r)."
    export KERNEL_VERSION=$(uname -r)
else
    echo "Using kernel version $1"
    export KERNEL_VERSION=$1

fi

cp ../ROSRCDdrivers/src/*.c src/
