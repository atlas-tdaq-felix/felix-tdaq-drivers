cd rpm/SOURCES/pkgs
python generate_dkms_makefile.py
./build-tarball.sh
cd ../../
rpmbuild --define "_topdir `pwd`" -bb --define "version ${RELEASE_TAG}" ./SPECS/tdaq_sw_for_Flx.spec
cd ..
mv rpm/RPMS/noarch/*.rpm dist/$RELEASE_NAME/