echo "Building library tarball..."
temp=dist/$RELEASE_NAME/tmp
mkdir -p $temp && mkdir -p $temp/$BINARY_TAG
cp -r installed/include $temp/$BINARY_TAG/
mkdir -p $temp/$BINARY_TAG/include/flxcard
cp kernel_modules/src/flx_common.h $temp/$BINARY_TAG/include/flxcard/
cp -r installed/$BINARY_TAG/lib $temp/$BINARY_TAG/
echo "Creating tar $RELEASE_NAME-$BINARY_TAG.tar.gz"
cd dist/$RELEASE_NAME/
tar -czf $RELEASE_NAME-lib-$BINARY_TAG.tar.gz -C tmp $BINARY_TAG/ 
rm -rf tmp
cd ../..
