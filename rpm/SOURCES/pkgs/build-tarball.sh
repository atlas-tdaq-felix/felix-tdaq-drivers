#!/bin/bash

##########################################################################################
#                                                                                        #
# This script populates a directory with TDAQ S/W. It is a precondition for making a RPM #
#                                                                                        #
#  Author: Markus Joos, CERN                                                             #
#                                                                                        #
##########################################################################################

if [[ -z "$CMAKE_PREFIX_PATH" ]]; then
    echo "CMAKE_PREFIX_PATH env variable is not defined. Set up CMake." 1>&2
    exit 1
fi

if [[ -z "$RELEASE_TAG" ]]; then
    echo "RELEASE_TAG env variable is not defined. Run setup_tags.sh." 1>&2
    exit 1
fi

ROOT_PATH=$(dirname $(dirname $CMAKE_PREFIX_PATH))
LIB_PATH=$ROOT_PATH/$BINARY_TAG

#######################
#create the directories
#######################
rm -rf tdaq_sw_for_Flx
mkdir -p tdaq_sw_for_Flx/cmem_rcc
mkdir -p tdaq_sw_for_Flx/io_rcc
mkdir -p tdaq_sw_for_Flx/ROSGetInput
mkdir -p tdaq_sw_for_Flx/DFDebug
mkdir -p tdaq_sw_for_Flx/rcc_error
mkdir -p tdaq_sw_for_Flx/flx
mkdir -p tdaq_sw_for_Flx/src
mkdir -p tdaq_sw_for_Flx/ROSRCDdrivers
mkdir -p tdaq_sw_for_Flx/drivers
mkdir -p tdaq_sw_for_Flx/script
mkdir -p tdaq_sw_for_Flx/lib64
mkdir -p tdaq_sw_for_Flx/bin


#############
#Header files
#############
cp -f $ROOT_PATH/cmem_rcc/cmem_rcc/cmem_rcc_drv.h                     tdaq_sw_for_Flx/cmem_rcc
cp -f $ROOT_PATH/cmem_rcc/cmem_rcc/cmem_rcc_common.h                  tdaq_sw_for_Flx/cmem_rcc
cp -f $ROOT_PATH/cmem_rcc/cmem_rcc/cmem_rcc.h                         tdaq_sw_for_Flx/cmem_rcc

cp -f $ROOT_PATH/io_rcc/io_rcc/io_rcc_driver.h                        tdaq_sw_for_Flx/io_rcc
cp -f $ROOT_PATH/io_rcc/io_rcc/io_rcc_common.h                        tdaq_sw_for_Flx/io_rcc
cp -f $ROOT_PATH/io_rcc/io_rcc/io_rcc.h                               tdaq_sw_for_Flx/io_rcc

cp -f $ROOT_PATH/rcc_error/rcc_error/rcc_error.h                      tdaq_sw_for_Flx/rcc_error

cp -f $ROOT_PATH/ROSGetInput/ROSGetInput/get_input.h                  tdaq_sw_for_Flx/ROSGetInput
cp -f $ROOT_PATH/DFDebug/DFDebug/DFDebug.h                            tdaq_sw_for_Flx/DFDebug
cp -f $ROOT_PATH/DFDebug/DFDebug/GlobalDebugSettings.h                tdaq_sw_for_Flx/DFDebug

cp -f $ROOT_PATH/ROSRCDdrivers/ROSRCDdrivers/tdaq_drivers.h           tdaq_sw_for_Flx/ROSRCDdrivers
cp -f $ROOT_PATH/ROSRCDdrivers/ROSRCDdrivers/xvc_pcie_driver.h        tdaq_sw_for_Flx/ROSRCDdrivers
cp -f $ROOT_PATH/ROSRCDdrivers/ROSRCDdrivers/xvc_pcie_ioctl.h         tdaq_sw_for_Flx/ROSRCDdrivers
cp -f $ROOT_PATH/ROSRCDdrivers/ROSRCDdrivers/xvc_pcie_user_config.h   tdaq_sw_for_Flx/ROSRCDdrivers

cp -f $ROOT_PATH/kernel_modules/src/flx_common.h                      tdaq_sw_for_Flx/flx


###################
#Driver source code
###################
cp -f $ROOT_PATH/ROSRCDdrivers/src/cmem_rcc.c                         tdaq_sw_for_Flx/src
cp -f $ROOT_PATH/ROSRCDdrivers/src/io_rcc.c                           tdaq_sw_for_Flx/src
cp -f $ROOT_PATH/ROSRCDdrivers/src/flx.c                              tdaq_sw_for_Flx/src


#############
#Driver tools
#############
cp -f ./Makefile_drivers                                              tdaq_sw_for_Flx/src/Makefile
cp -f ./dkms.conf                                                     tdaq_sw_for_Flx
cp -f ./drivers_flx                                                   tdaq_sw_for_Flx/script
cp -f ./drivers_flx_sd.service                                        tdaq_sw_for_Flx/script


###############
#tdaq libraries
###############
cp -f $LIB_PATH/rcc_error/librcc_error.so                             tdaq_sw_for_Flx/lib64
cp -f $LIB_PATH/io_rcc/libio_rcc.so                                   tdaq_sw_for_Flx/lib64
cp -f $LIB_PATH/cmem_rcc/libcmem_rcc.so                               tdaq_sw_for_Flx/lib64
cp -f $LIB_PATH/DFDebug/libDFDebug.so                                 tdaq_sw_for_Flx/lib64
cp -f $LIB_PATH/ROSGetInput/libgetinput.so                            tdaq_sw_for_Flx/lib64


#########################################
# libraries required by the test programs
#########################################
LIBSTDCPP=$(ldd $LIB_PATH/cmem_rcc/test_cmem_rcc | grep libstdc | awk '{print $3 " "}')

cp -f $LIBSTDCPP                                                      tdaq_sw_for_Flx/lib64


#########################################################
# some applications for testing the drivers and libraries
#########################################################
cp -f $LIB_PATH/cmem_rcc/cmem_rcc_bpa_test                            tdaq_sw_for_Flx/bin
cp -f $LIB_PATH/cmem_rcc/test_cmem_rcc                                tdaq_sw_for_Flx/bin

tar czf  ../tdaq_sw_for_Flx-$RELEASE_TAG-src.tar.gz --directory tdaq_sw_for_Flx .
