import os

#Generate dkms.conf
dmks_conf = "PACKAGE_NAME=\"tdaq_sw_for_Flx\"\n"
dmks_conf += "PACKAGE_VERSION=\"" + str(os.environ['RELEASE_TAG']) + "\"\n"

dmks_conf_append = """
REMAKE_INITRD="no"

BUILT_MODULE_NAME[0]="io_rcc"
BUILT_MODULE_LOCATION[0]="drivers"
DEST_MODULE_LOCATION[0]="/extra"
MAKE[0]='make -C src'

BUILT_MODULE_NAME[1]="flx"
BUILT_MODULE_LOCATION[1]="drivers"
DEST_MODULE_LOCATION[1]="/extra"
MAKE[1]='make -C src'

BUILT_MODULE_NAME[2]="cmem_rcc"
BUILT_MODULE_LOCATION[2]="drivers"
DEST_MODULE_LOCATION[2]="/extra"
MAKE[2]='make -C src'

AUTOINSTALL="yes"
CLEAN='make clean -C src'
"""

dmks_conf += dmks_conf_append

with open('dkms.conf', 'w') as f:
  f.write(dmks_conf)


#Generate RPM makefile
makefile = """KERNEL_VERSION := $(shell uname -r)
KERNEL_MODLIB  := /lib/modules/$(KERNEL_VERSION)
KERNEL_SOURCES := $(shell test -d $(KERNEL_MODLIB)/source && echo $(KERNEL_MODLIB)/source || echo $(KERNEL_MODLIB)/build)

CPPFLAGS      += -I$(PWD)/../flx -I$(PWD)/.. -DBUILD_64_BIT -DCOMPILE_DRIVER -DDRIVER_ERROR -DDRIVER_DEBUG -DRELEASE_NAME=\\"${RELEASE_NAME}\\" -DCVSTAG=\\"$(CVSTAG)\\"
EXTRA_CFLAGS  := -I$(PWD)/../flx -I$(PWD)/.. -DBUILD_64_BIT -DCOMPILE_DRIVER -DDRIVER_ERROR -DDRIVER_DEBUG -DRELEASE_NAME=\\"${RELEASE_NAME}\\" -DCVSTAG=\\"$(CVSTAG)\\" -DCMEM_RCC_TAG=\\"$(CMEM_RCC_TAG)\\" -DIO_RCC_TAG=\\"$(IO_RCC_TAG)\\" -DFLX_TAG=\\"$(FLX_TAG)\\" 

"""

makefile += "RELEASE_NAME := \"" + os.environ['RELEASE_NAME'] + "\" \n"
makefile += "CVSTAG := \"" + os.environ['GIT_BUILD_HASH'] + "\" \n"
makefile += "FLX_TAG := \"" + os.environ['GIT_RCD_HASH'] + "\" \n"
makefile += "CMEM_RCC_TAG := \"" + os.environ['GIT_CMEM_HASH'] + "\" \n"
makefile += "IO_RCC_TAG := \"" + os.environ['GIT_IO_HASH'] + "\" \n"

makefile += """

obj-m   := cmem_rcc.o io_rcc.o flx.o
KDIR  := $(KERNEL_SOURCES)
PWD := $(shell pwd)
default:
\t$(MAKE) -C $(KDIR) M=$(PWD) RELEASE_NAME="\\"$(RELEASE_NAME)\\"" CVSTAG="\\"$(CVSTAG)\\"" FLX_TAG="\\"$(FLX_TAG)\\"" CMEM_RCC_TAG="\\"$(CMEM_RCC_TAG)\\"" IO_RCC_TAG="\\"$(IO_RCC_TAG)\\"" modules
\tmkdir -p ../drivers 
\tmv cmem_rcc.ko ../drivers/
\tmv io_rcc.ko ../drivers/
\tmv flx.ko ../drivers/
clean:
\t$(RM) cmem_rcc.ko.unsigned cmem_rcc.mod.c cmem_rcc.mod.o cmem_rcc.o modules.order Module.symvers .cmem_rcc* .tmp_versions/*
"""

with open('Makefile_drivers', 'w') as m:
  m.write(makefile)

