echo "=== BUILD TAGS ==="
rname="felix-drivers-00-00-00"
rtag="0.0.0"

release_name_to_tag () {
  local num=$(echo $rname | sed -e "s/^felix-drivers-//")
  local major=$(( $(echo $num | cut -d '-' -f 1 )))
  local minor=$(( $(echo $num | cut -d '-' -f 2 )))
  local bugfix=$(( $(echo $num | cut -d '-' -f 3 )))
  rtag="$major.$minor.$bugfix"
  echo "Release tag:        $rtag"
}

if [ -z $CI_COMMIT_TAG ]; then
    echo "Release name:       felix-drivers-00-00-00" 
else
    echo "Release name:       $CI_COMMIT_TAG"
    rname=$CI_COMMIT_TAG
fi

release_name_to_tag
export RELEASE_NAME=$rname
export RELEASE_TAG=$rtag

export GIT_RCD_BRANCH=$(cd ROSRCDdrivers && git rev-parse --abbrev-ref HEAD)
export GIT_RCD_HASH=$(cd ROSRCDdrivers && git describe --always --dirty)
echo "RCD drivers:        $GIT_RCD_HASH"

export GIT_CMEM_BRANCH=$(cd cmem_rcc && git rev-parse --abbrev-ref HEAD)
export GIT_CMEM_HASH=$(cd cmem_rcc && git describe --always --dirty)
echo "CMEM library:       $GIT_CMEM_HASH"

export GIT_IO_BRANCH=$(cd io_rcc && git rev-parse --abbrev-ref HEAD)
export GIT_IO_HASH=$(cd io_rcc && git describe --always --dirty)
echo "IO_RCC library:     $GIT_IO_HASH"

export GIT_BUILD_BRANCH=$(git rev-parse --abbrev-ref HEAD)
export GIT_BUILD_HASH=$(git describe --always --dirty)
echo "felix-tdaq-drivers: $GIT_BUILD_HASH"

echo "=================="
